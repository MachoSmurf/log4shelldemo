--
-- Database: `dutchresearchgroup`
--
CREATE DATABASE IF NOT EXISTS `dutchresearchgroup` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `dutchresearchgroup`;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
                         `id` int(11) NOT NULL AUTO_INCREMENT,
                         `username` varchar(255) NOT NULL,
                         `email` varchar(255) NOT NULL,
                         `first_name` varchar(255) NOT NULL,
                         `last_name` varchar(255) NOT NULL,
                         `date_of_birth` date NOT NULL,
                         `password` varchar(255) NOT NULL,
                         PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO users (username, email, first_name, last_name, date_of_birth, password)
VALUES
    ('john.dejong', 'john.dejong@dutchresearchgroup.com', 'John', 'De Jong', '1960-03-15', MD5('password123')),
    ('jane.vanburen', 'jane.vanburen@dutchresearchgroup.com', 'Jane', 'Van Buren', '1962-05-22', MD5('123456')),
    ('alice.smith', 'alice.smith@dutchresearchgroup.com', 'Alice', 'Smith', '1964-07-30', MD5('qwerty')),
    ('bob.jansen', 'bob.jansen@dutchresearchgroup.com', 'Bob', 'Jansen', '1966-09-08', MD5('letmein')),
    ('charlie.visser', 'charlie.visser@dutchresearchgroup.com', 'Charlie', 'Visser', '1968-11-16', MD5('football')),
    ('daniel.wilson', 'daniel.wilson@dutchresearchgroup.com', 'Daniel', 'Wilson', '1971-01-24', MD5('baseball')),
    ('emily.deboer', 'emily.deboer@dutchresearchgroup.com', 'Emily', 'De Boer', '1973-03-04', MD5('iloveyou')),
    ('frank.davids', 'frank.davids@dutchresearchgroup.com', 'Frank', 'Davids', '1975-06-12', MD5('sunshine')),
    ('grace.bakker', 'grace.bakker@dutchresearchgroup.com', 'Grace', 'Bakker', '1977-08-20', MD5('princess')),
    ('henry.martinez', 'henry.martinez@dutchresearchgroup.com', 'Henry', 'Martinez', '1979-10-28', MD5('monkey')),
    ('ivy.vandenberg', 'ivy.vandenberg@dutchresearchgroup.com', 'Ivy', 'Van den Berg', '1981-12-06', MD5('dragon')),
    ('jake.clark', 'jake.clark@dutchresearchgroup.com', 'Jake', 'Clark', '1984-02-14', MD5('superman')),
    ('kathy.meijer', 'kathy.meijer@dutchresearchgroup.com', 'Kathy', 'Meijer', '1986-04-23', MD5('batman')),
    ('leo.lee', 'leo.lee@dutchresearchgroup.com', 'Leo', 'Lee', '1988-07-01', MD5('trustno1')),
    ('mia.vanderlinden', 'mia.vanderlinden@dutchresearchgroup.com', 'Mia', 'Van der Linden', '1990-09-09', MD5('jordan23')),
    ('nolan.hall', 'nolan.hall@dutchresearchgroup.com', 'Nolan', 'Hall', '1992-11-17', MD5('harley')),
    ('olivia.mulder', 'olivia.mulder@dutchresearchgroup.com', 'Olivia', 'Mulder', '1994-01-26', MD5('tinkerbell')),
    ('peter.dejong', 'peter.dejong@dutchresearchgroup.com', 'Peter', 'De Jong', '1996-04-04', MD5('scooby')),
    ('quincy.vanheck', 'quincy.vanheck@dutchresearchgroup.com', 'Quincy', 'Van Heck', '1998-06-13', MD5('mustang')),
    ('rachel.king', 'rachel.king@dutchresearchgroup.com', 'Rachel', 'King', '2000-08-21', MD5('shadow')),
    ('sam.wright', 'sam.wright@dutchresearchgroup.com', 'Sam', 'Wright', '2002-10-30', MD5('cheese')),
    ('tina.janssen', 'tina.janssen@dutchresearchgroup.com', 'Tina', 'Janssen', '2004-12-09', MD5('chocolate')),
    ('ursula.hill', 'ursula.hill@dutchresearchgroup.com', 'Ursula', 'Hill', '2007-03-18', MD5('michelle')),
    ('victor.vanwezel', 'victor.vanwezel@dutchresearchgroup.com', 'Victor', 'Van Wezel', '2009-05-27', MD5('pepper')),
    ('wendy.groen', 'wendy.groen@dutchresearchgroup.com', 'Wendy', 'Groen', '2011-08-05', MD5('password'));
