package de.predic8;

import spark.Spark;

import java.io.File;
import java.nio.file.Files;

public class HttpServer {

    public static void main(String[] args) {
        Spark.ipAddress("0.0.0.0");
        Spark.port(8080);
        System.out.println("Spark Started");

        Spark.get("/BadCode.class", (req, res) -> {
            System.out.println("Got request:" + req.toString());
            return Files.readAllBytes(new File("target/classes/BadCode.class").toPath());
        });
    }
}